/**
 * bigbuy buttons plugin
 **/

(function($) {

var defaults = {
  test_code: 'a',
  checked: ['angela'],
  order_button_text: 'ORDER NOW',
  extended_shipping: false,
  shipping_info: 'Ships next working day',
  free_shipping: ['Free shipping','&nbsp;'],
  not_free_shipping:  ['Select 2 toys for', 'free shipping'],
  handles: {
    tom: 'talking-tom-superstar',
    angela: 'talking-angela-superstar',
    ginger: 'talking-ginger-superstar'
  },
  tests : {
    a: {
      toy_name: false
    },

    b: {
      toy_name: false
    },

    c: {
      toy_name: true,
      order_button_text: 'ORDER ANGELA'
    },

    d: {
      checked: ['angela', 'tom', 'ginger'],
      toy_name: false
    },

    e: {
      checked: ['angela', 'tom', 'ginger'],
      toy_name: false,
      extended_shipping: true
    }
  }
};

var BIG_BUY_HTML = 
  "<div class='big-buy-outer'>" +
    "<div class='big-buy-inner'>" +

      "<div class='big-buy-order goog-inline-block'>" +
        "<div class='fancy-button'>" +
          "<img src='/wp-content/themes/o7-custom/images/starsmall2.png' class='fancy-button-stars1' alt='stars'>" +
          "<img src='/wp-content/themes/o7-custom/images/starsmall2.png' class='fancy-button-stars2' alt='stars'>" +
          "<a class='track-click currency currency-eur' href='http://talkingfriends.eu/cart?superstar=talking-ginger-superstar'>ORDER NOW</a>" +
          "<a class='track-click currency currency-usd' href='http://talkingfriends.com/cart?superstar=talking-ginger-superstar'>ORDER NOW</a>" +
        "</div>" +
      "</div>" +

      "<div class='big-buy-choices'>" +
        "<div class='big-buy-choice tom' data-toy='tom'><span class='box'></span><span class='mark'></span>Tom</div>" +
        "<div class='big-buy-choice angela' data-toy='angela'><b class='box'></b><b class='mark'></b>Angela</div>" +
        "<div class='big-buy-choice ginger' data-toy='ginger'><b class='box'></b><b class='mark'></b>Ginger</div>" +
      "</div>" +
      
      "<div class='big-buy-price'>" +
        "<span class='currency currency-usd'>$44.99&nbsp;</span>"+
        "<span class='currency currency-eur'>&euro;44.99&nbsp;</span>"+
        "<span class='big-buy-per-toy'><br/>per toy</span>" +
      "</div>" +

      "<div class='big-buy-stock'></div>" +
    "</div>" +
  "</div>" +
  
  "<div class='big-buy-order-other'>" +
  "<p>To order TOM, " +
  "<a class='currency currency-eur' data-event='click-order-tom-other' href='http://talkingfriends.eu/cart?superstar=talking-tom-superstar'>click here</a>" +
  "<a class='currency currency-usd' data-event='click-order-tom-other' href='http://talkingfriends.com/cart?superstar=talking-tom-superstar'>click here</a>.</p>" +
  "<p>To order GINGER, " + 
  "<a class='currency currency-eur' data-event='click-order-ginger-other' href='http://talkingfriends.eu/cart?superstar=talking-ginger-superstar'>click here</a>" +
  "<a class='currency currency-usd' data-event='click-order-ginger-other' href='http://talkingfriends.com/cart?superstar=talking-ginger-superstar'>click here</a>.</p>" +
  "</div>"

var FREE_SHIPPING_COUNTRIES = [
		'AT', 'BE', 'BG', 'CY', 'CZ', 'DE',
		'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU',
		'IE', 'IT', 'LT', 'LU', 'LV', 'MC', 'MT', 'NL', 'PO',
		'PT', 'RO', 'SE', 'SI', 'SK', 'SM', 'US'];

var is_free_shipping_possible = function(country_code) { 
  var cc = country_code || '';
  return (jQuery.inArray(cc.toUpperCase(), FREE_SHIPPING_COUNTRIES) != -1);
}


var init_ = function(options) {

  /** returns test specific or default param **/
  var get_param = function(name) { return test[name] || settings[name]; }

  var
      $that = $(this),
      settings = $.extend({}, defaults, options),
      test_class = /test-([a-e])/.exec($(this).attr('class')),
      test_code = test_class && test_class[1] || settings.test_code,
      test = settings.tests[test_code],
      toy_match = /(tom|angela|ginger)/.exec($that.attr('class')),
      toy = toy_match && toy_match[1],
      checked = toy && [toy] || get_param('checked'),
      order_button_text = toy && 'order '+toy || get_param('order_button_text'),
      currency = '$',
      country_code = 'US',
      possible_free_shipping = false,
      data_event_node = $that.attr('data-event') && $that ||  $that.parents('*[data-event]')[0];

  // replaces 2nd parameter with current version of test split (vma parameter)
  // but only to nodes that has abtest class
  if (data_event_node && $that.hasClass('abtest')) {
    var parts = data_event_node.attr('data-event').split(';');
    parts[1] = test_code;
    data_event_node.attr('data-event', parts.join(";"))
  }
      

  /**
   * returns array oflines for shipping text 
   * f (items_selected, free_shipping_possible, extended_shipping)
   **/
  var get_shipping_text = function(items_selected) {
    var extended_shipping = get_param('extended_shipping'),
        free_shipping_possible = is_free_shipping_possible(country_code),
        line_1 = get_param('shipping_info');

    if (extended_shipping) {
      if (free_shipping_possible) {
        return (items_selected > 1) 
          ? $.merge([line_1], get_param('free_shipping')) 
          : $.merge([line_1], get_param('not_free_shipping'));
      } else {
        return [line_1, '&nbsp;', '&nbsp;'];
      }
    } else {
      return [line_1];
    }
  }

  var redraw = function() {
    var superstars = [];

    $.each(checked, function(index, toy) {
      superstars.push(settings.handles[toy])});

    var query = 'referrer=tfsuperstar.com&superstar=' + (superstars.join(",") || settings.handles['angela']);

    $('.big-buy-stock', $that).html(get_shipping_text(superstars.length).join('<br/>'));
    $('.big-buy-order a', $that)
      .text(order_button_text)
      .each(function(el){ 
        this.href = /(.*)\?/.exec(this.href)[0] + query
      });


    $.each(checked, function(idx, toy) {
      $('.big-buy-choices .'+toy, $that).addClass('checked');
    });
  }

  var check_toggled = function() {
    checked = [];
    $('.big-buy-choices .checked', $that).each(function() {
      checked.push(this.getAttribute('data-toy'));
    });
    redraw();
  };

  $that.html(BIG_BUY_HTML);

  $('.big-buy-choice', $that).click(function() {
    $(this).toggleClass('checked'); 
    check_toggled();
  });

  $(document).on('changeUser', function(event, data) {
    // currency && country_code are closures
    currency = data.currency;
    country_code = data.country_code || '';
    redraw();
  });
  redraw();
}


var methods = {}

methods.init = function(options) {  
  return this.each(function(options) {init_.call(this, options)})
};

// Method calling logic
$.fn.bigbuy = function(method) {
  if ( methods[method] ) {
    return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
  } else if ( typeof method === 'object' || ! method ) {
    return methods.init.apply( this, arguments );
  } else {
    $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
  }    
}

/**
 * get-s value of abtest param either from 
 * window.location or cookie 
 **/ 
$.fn.bigbuy.get_abtest = function() {
  var abtest_match = /.*vma=['"]*([a-z]+)['"]*/.exec(window.location.href),
      abtest = abtest_match && abtest_match[1]
      abtest_cookie =$.cookie('vma');

  if (abtest && !abtest_cookie) {
    $.cookie('abtest', abtest);
    return abtest;
  }

  return abtest || abtest_cookie;
}

})(jQuery); 
