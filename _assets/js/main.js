$(function() {

  // cookie data in 'vac' ar json serialized
  $.cookie.json = true;

  // getTime() number of milliseconds since 1970/01/01
  var MS_IN_DAY = 1000 * 3600 * 24;
  var MAX_DAYS_TO_LAUNCH = 6;
  var VOTES_COOKIE = '__vac';
  var VOTING_START = new Date("14 Dec 2012 13:00:00 PST");
  var LAUNCH_TIME = new Date("20 Dec 2012 13:00:00 PST");


  /**************************** css animation  *****************************/

  var checkIfCssAnimationSupported = function() {
    var animation = false,
    animationstring = 'animation',
    keyframeprefix = '',
    domPrefixes = 'Webkit Moz O ms Khtml'.split(' '),
    elm = document.documentElement,
    pfx  = '';

    if( elm.style.animationName ) { animation = true; }    

    if( animation === false ) {
      for( var i = 0; i < domPrefixes.length; i++ ) {
        if( elm.style[ domPrefixes[i] + 'AnimationName' ] !== undefined ) {
          pfx = domPrefixes[ i ];
          animationstring = pfx + 'Animation';
          keyframeprefix = '-' + pfx.toLowerCase() + '-';
          animation = true;
          break;
        }
      }
    }
    return animation;
  }

  var isCssAnimationSupported = checkIfCssAnimationSupported();

  /**************************** countdown *****************************/

  var SHARING = {
    'fb' : {
      'tom': 'Superstars Hit Hollywood\n I just voted for Tom’s Superstar look for his debut performance at Hollywood & Highland on December 20!',
      'angela': 'I just voted for Angela’s Superstar look for her debut performance at Hollywood & Highland on December 20!',
      'ginger': 'I just voted for Ginger’s Superstar look for his debut performance at Hollywood & Highland on December 20!'
    },

    'tw' : {
      'tom': 'I just voted for Tom’s Superstar look for his debut performance at Hollywood & Highland on December 20 #tfsuperstar',
      'angela': 'I just voted for Angela’s Superstar look for her debut performance at Hollywood & Highland on December 20 #tfsuperstar',
      'ginger': 'I just voted for Ginger’s Superstar look for his debut performance at Hollywood & Highland on December 20 tfsuperstar'
    },

    'pt': {
      'tom': 'I just voted for Tom’s Superstar look for his debut performance at Hollywood & Highland on December 20!',
      'angela': 'I just voted for Angela’s Superstar look for her debut performance at Hollywood & Highland on December 20!',
      'ginger': 'I just voted for Ginger’s Superstar look for his debut performance at Hollywood & Highland on December 20!'
    },

    'tb': {
      'tom': 'I just voted for Tom’s Superstar look for his debut performance at Hollywood & Highland on December 20! Go vote: www.tfsuperstar.com/hollywood',
      'angela': 'I just voted for Angela’s Superstar look for her debut performance at Hollywood & Highland on December 20! Go vote: www.tfsuperstar.com/hollywood',
      'ginger': 'I just voted for Ginger’s Superstar look for his debut performance at Hollywood & Highland on December 20! Go vote: www.tfsuperstar.com/hollywood'
    }
  };

  var initSocials = function() {
    $.each(['tom', 'angela', 'ginger'], function(index, superstar) {
      initSocial(superstar, '1');
      initSocial(superstar, '2');
    });
  }

  FB_URL = "https://www.facebook.com/dialog/feed?";
  TW_URL = "http://twitter.com/share?";
  PT_URL = "http://pinterest.com/pin/create/button/?";
  TB_URL = "http://www.tumblr.com/share/photo?";
  ASSETS = "http://beta.tfsuperstar.com/hollywood/assets/";

  var initSocial = function(superstar, outfit) {

    var id_map = {'tom': 'tom', 'angela': 'ang', 'ginger': 'gin'},
    id_ = '#' + id_map[superstar] + 'o' + outfit,
    fb = $(id_ + ' .vsharingfb'),
    tw = $(id_ + ' .vsharingtw'),
    pt = $(id_ + ' .vsharingpt'),
    tb = $(id_ + ' .vsharingtb');

    var fb_param = {
      'app_id': (document.domain == 'beta.tfsuperstar.com') ? '126774660815576' : '126774660815576',
      'link': 'http://' + document.domain + '/hollywood/',
      'picture': ASSETS + 'fb_' + superstar + outfit + '.jpg',
      'description': SHARING['fb'][superstar],
      'name': "Superstars Hit Hollywood",
      'caption': " ",
      'redirect_uri': 'http://' + document.domain + '/hollywood/'
    };

    var tw_param = {
      'text': SHARING['tw'][superstar]
    };
    var pt_param = {
      'url': window.location.href,
      'media': ASSETS + 'tb_' + superstar + outfit + '.jpg',
      'description': SHARING['tb'][superstar]
    };
    var tb_param = {
      'source': ASSETS + 'tb_' + superstar + outfit + '.jpg',
      'caption': SHARING['tb'][superstar]
    };

    // tb
    $(fb).attr('href', FB_URL + $.param(fb_param));
    $(tw).attr('href', TW_URL + $.param(tw_param)).attr('target', '_blank');
    $(pt).attr('href', PT_URL + $.param(pt_param)).attr('target', '_blank');
    $(tb).attr('href', TB_URL + $.param(tb_param)).attr('target', '_blank');

  }

  $('.vsharingfb, .vsharingtw, .vsharingpt, .vsharingtb').click(function() {
    var $this = $(this),
    social_site = $this.text(),
    outfit = $this.parents('.overlay').attr('id');

    _gaq.push(['_trackEvent', 'Hollywood', 'Share on ' + social_site, outfit, 0, true])
  })

  initSocials();



  /**************************** voting *****************************/

  var doVote = function (event) {
    var classes = event.target.getAttribute('class').split(' '),
        vote_id = $.grep(classes,
        function(f) {return /v[tag][12]/.test(f)})[0],
        superstar = vote_id[1],
        outfit_num = vote_id[2],
        vote_id_1 = 'v' + superstar + '1',
        vote_id_2 = 'v' + superstar + '2',
        votes_already_cast = $.cookie(VOTES_COOKIE) || {},
        now = new Date().getTime();


    var last_voting_time = now - (
      Math.max(votes_already_cast[vote_id_1] || 0, votes_already_cast[vote_id_2] || 0));
      var tnx_for_vote = {'a': 'ang', 't': 'tom', 'g': 'gin'}[superstar] + 'o' + outfit_num;

      if ( last_voting_time > MS_IN_DAY) {
        _gaq.push(['_trackEvent', 'Hollywood', 'Vote', vote_id, 0, true]);
        $.cookie(VOTES_COOKIE, null, {'path': '/'});
        votes_already_cast[vote_id] = now;
        $.cookie(VOTES_COOKIE, votes_already_cast, {'expires':1, 'path':'/'});
      } else {
        _gaq.push(['_trackEvent', 'Hollywood', 'Vote', 're_' + vote_id, 0, true]);
        //_gaq.push(['_trackEvent', 'Hollywood', 'Vote', vote_id, 0, true])
        $('#' + tnx_for_vote + " h1").text("Sorry, one vote per day!");
      }
      // show thank you
      $('#' + tnx_for_vote).show();

      // if mobile, scroll to next
      if ($(window).width() < 767) {
        var next = $(event.target).parents('.vote').next('.vote');

        next.length && $('html,body').animate({
          scrollTop: next.offset().top
        }, 2000);
      }
  };
  // vote handler
  $('.action-vote').live('click', doVote);

  var votesAlreadyAnimated = false;

  var setScroller = function(id, left, right) {
    var total = left + right || 1,
        left_p = Math.round(left * 100 / total),
        right_p = Math.round(right * 100 / total),
        left_score = $("#" + id + "vbar .scorel"),
        left_bar = $("#" + id + "vbar .pointer:even"),
        right_score = $("#" + id + "vbar .scorer"),
        right_bar = $("#" + id + "vbar .pointer:odd"),
        scalled_right_p = (right_p * 33.5 / 100),
        scalled_left_p = (left_p * 33.5 / 100);

    left_score.removeClass(id + 'sel').text(left_p + '%');
    left_bar.
      addClass(id).
      removeClass(id + 'win').
      attr('style', 'right: 51%; width: ' + scalled_left_p + '%');

    right_score.removeClass(id + 'sel').text(right_p + '%');
    right_bar.
      addClass(id).
      removeClass(id + 'win').
      attr('style', 'left: 51%; width: ' + scalled_right_p + '%');

    if (left > right) {
      left_score.addClass(id+'sel');
      left_bar.addClass(id+'win');
    }
    if (left < right) {
      right_score.addClass(id+'sel');
      right_bar.addClass(id+'win');
    }
  }

  var setCounter = function(superstar, left, right) {
      var vbar = $('#' + superstar +'vbar');
      $('.left .total-votes', vbar).text(left);
      $('.right .total-votes', vbar).text(right);

      $('.left', vbar).removeClass(superstar + 'sel');
      $('.right', vbar).removeClass(superstar + 'sel');

      if (left > right)
        $('.left', vbar).addClass(superstar + 'sel');

      if (right > left)
        $('.right', vbar).addClass(superstar + 'sel');
  }

  function getUTCTime(date) {
    return ~~((+date / 1000) - date.getTimezoneOffset() * 60);
  }

  var calcSpeed = function(votes) {
    var old_votes = votes['old_votes'],
    current_votes = votes['current_votes'],
    time_passed = getUTCTime(new Date) - Date.parse(votes['last_access']) / 1000;
    time_passed_from_voting_start = getUTCTime(new Date) - Date.parse(VOTING_START) / 1000;
    delta_t = (current_votes['timestamp'] - old_votes['timestamp']) / 1000,
    calcFutureVotes = function(of) {
      var
      v2 = parseInt(current_votes[of] || 0),
      v1 = parseInt(old_votes[of] || 0),
      average_speed = v2 / time_passed_from_voting_start, 
      current_speed = (v2 - v1) / delta_t || average_speed,
      speed = (current_speed < average_speed && current_speed > 0.1) ? current_index : average_speed,
      future_votes = speed * time_passed + v2;

      return Math.round((future_votes > v2) ? future_votes : v2, 0);
    };
    return {
      'vt1': calcFutureVotes('vt1'),
      'vt2': calcFutureVotes('vt2'),
      'va1': calcFutureVotes('va1'),
      'va2': calcFutureVotes('va2'),
      'vg1': calcFutureVotes('vg1'),
      'vg2': calcFutureVotes('vg2')
    }
  }

  var displayVotes = function(response) {
    future_votes = calcSpeed(response);
    $.each(['a', 't', 'g'], function(idx, superstar) {
      var left = parseInt(future_votes['v' + superstar + '1'] || 0),
      right = parseInt(future_votes['v' + superstar + '2'] || 0);

      !votesAlreadyAnimated && setScroller(superstar, left, right);
      setCounter(superstar, left, right);
    });
    votesAlreadyAnimated = true;
    setTimeout(function() {
      displayVotes(response);
    }, 1000)
  };


  $.ajax({
    url: '/hollywood/votes.json', 
    dataType: "json",
    error: function(response) {
      displayVotes({});
    },
    success: function(response) {
      displayVotes(response);
    }
  });

  var ON_CLICK = [
    ['.vt1', '#tomo1', 'show'],
    ['.vt2', '#tomo2', 'show'],
    ['.va1', '#ango1', 'show'],
    ['.va2', '#ango2', 'show'],
    ['.vg1', '#gino1', 'show'],
    ['.vg2', '#gino2', 'show'],
    ['#tom1close', '#tomo1', 'hide'],
    ['#tom2close', '#tomo2', 'hide'],
    ['#ang1close', '#ango1', 'hide'],
    ['#ang2close', '#ango2', 'hide'],
    ['#gin1close', '#gino1', 'hide'],
    ['#gin2close', '#gino2', 'hide']
  ];

  $.each(ON_CLICK, function(index, a) {
    $(a[0]).click(function(e) {
      e.preventDefault();
      $(a[1])[a[2]]();
    });
  });

  var diaryHandler = function() {
    var $this = $(this),
    width = $this.width(),
    items = $(".ditem", this),
    sccont = $('.sccont', this),
    dbleft = $('.dbleft', this),
    dbright = $('.dbright', this),
    scrollwr = $('.scrollwr', $this),
    superstar = $this.attr('id'),
    current_index = 0;

    var onResize = function() {
      width = $this.width();

      items.width(width).css('left', width + "px");
      //sccont.width(width).scrollLeft(current_index);
    }

    onResize();
    $(window).resize(onResize);


    var circular = function(x, low, high) {
      return (x < low) ? high : (x > high) ? low : x;
    }

    var scroll = function(move) {
      //console && console.log("Scroll current index: " + current_index + ", " + move);
      var new_position = circular(current_index + move, 0, items.length-1),
      left = (- width * new_position) + "px";


      //isAnimating = true;
      if (isCssAnimationSupported) {
        scrollwr.css('left', left);
      } else {
        scrollwr.animate({'left': left}, 600, 'swing', function() {
        });
      }
      current_index = new_position;

      dbleft.show();
      dbright.show();
      if (current_index > 0) { dbright.show()}

      if (current_index == 0) {dbleft.hide();}
      //console && console.log("Scroll index: " + new_position);
      //console && console.log("Scroll left: " + left);
      //console && console.log("Scroll left: " + scrollwr.attr('id'));
    }

    dbright.show().click(function(e) {e.preventDefault(); scroll(1)});
    dbleft.hide().click(function(e) {e.preventDefault(); scroll(-1)});

  };

  $('.diary').each(diaryHandler);

  $("#tomdiary").wipetouch({
    preventDefault:false,
    wipeLeft: function(e) {$("#tomr").click();},
    wipeRight: function(e) { $("#toml").click();}
  });

  $("#angeladiary").wipetouch({
    preventDefault:false,
    wipeLeft: function(e) {$("#angr").click(); },
    wipeRight: function(e) { $("#angl").click();}
  });

  $("#gingerdiary").wipetouch({
    preventDefault:false,
    wipeLeft: function(e) {$("#ginr").click(); },
    wipeRight: function(e) { $("#ginl").click();}
  });

});

$(document).ready(function () {
	if (window.innerWidth < 769) {
		$('body').animate({
		    scrollTop: $("#tom").offset().top
		}, 1000);
	}
});
