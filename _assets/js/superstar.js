(function($) {

  $.fn.superstar = function() {
  /** constants **/
  var PARAM_KEYS = ['ref', 'user_country_code', 'user_country_name',
    'user_currency', 'utm_source', 'utm_medium', 'utm_term', 'utm_content',
    'utm_campaign', 'gclid'];

  var CAMPAIGN_COOKIES = ['utm_source', 'utm_medium', 'utm_term',
    'utm_content', 'utm_campaign', 'gclid'];

  /** outer function variables **/
  var g_params = {};
  var g_tracked_events = {}; // set of already tracked events
  var g_cookies = {};
  var g_query = {};
  var g_params = {};

  /** Main entry point **/
  var main = function() {
    set_user_currency('USD', 'us');
    ui_tweaks();

    g_query = get_query(window.location.href);
    is_campaing_param(g_query) && clear_utm_cookies();

    g_cookies = get_cookies();

    // query values overrides cookie values
    $.extend(g_params, g_cookies, g_query);

    $.cookie('test_cookie', 'cookie_value', { path: '/' });
    g_cookies_enabled = $.cookie('test_cookie') === 'cookie_value';

    set_cookies(g_params, get_diff_keys(g_query, g_cookies));

    // if there is no cookie user_country_code, do an async request
    geoip();

    // augment link on click
    $('*[href]').live('click', onClick);

    register_events();

    var href = window.location.href;
    // redirects in beta
    /*
    if (/beta\.tfsuperstar/.test(href) ||
        /draagle/.test(href)) {
        redirect_talkingshop('test-talkingshop.myshopify.com'); 
    }*/

    $('.stars_selection img').live('click', onClickStarsSelection);
  }


  /* Live image click */
  var onClickStarsSelection = function(event) {
    var offset = $(this).offset(),
        width = $(this).width() || 1,
        height = $(this).height() || 1,
        relativeX = (event.pageX - offset.left) * 1000.0 / width,
        relativeY = (event.pageY - offset.top) * 1000.0 / height,
        link = '/talking-ginger-superstar';

    if (relativeX < 350) {
      link = '/talking-angela-superstar';
    } else if (relativeX < 650) {
      link = '/talking-tom-superstar';
    }
    
    window.location.href = link;
  }

  // redirect all talking shop links to new domain
  var redirect_talkingshop = function(new_domain) {
    $('a').each(function() {
      var href = this.getAttribute('href') || '';
      if (href) {
        href = href.replace(/talkingfriends.com/g, new_domain);
        href = href.replace(/talkingfriends.eu/g, new_domain);
        this.href = href;
      }
    });
  }

  /** cookie utils **/

  var get_cookies = function() {
    var obj = {};
    $.each(PARAM_KEYS, function(index, key) { obj[key] = $.cookie(key)});
    return obj;
  }

  var get_cookie_expires = function(key) {
    return $.inArray(key, CAMPAIGN_COOKIES) && 1 || 356;
  }

  var clear_utm_cookies = function() {
    $.each(CAMPAIGN_COOKIES, function(index, key) {
      $.cookie(key, null, {'expires': 0, 'path': '/'});
    });
  }

  var set_cookies = function(obj, keys_to_set) {
    $.each(keys_to_set, function(index, key) {
      var val = obj[key];
      var expires = get_cookie_expires(key);
      val && $.cookie(key, val, {'expires': expires, 'path': '/'});
    })};


  /** geoip functions **/
  var geoip = function() {

    if (!g_params['user_currency']) {
      jQuery.getScript('//j.maxmind.com/app/country.js', function() {
        var user_country_code = geoip_country_code(),
            user_country_name = geoip_country_name(),
            user_currency = get_user_currency(user_country_code) || 'USD';

        if (user_country_code) {
          g_params['user_country_name'] = user_country_name;
          g_params['user_country_code'] = user_country_code;
          g_params['user_currency'] = user_currency;

          set_cookies(g_params, ['user_country_code',
            'user_country_name', 'user_currency']);
        }
        set_user_currency(user_currency, user_country_code);
      });
    } else {
        set_user_currency(g_params['user_currency'], g_params['user_country_code']);
    }

  }


  /** other utils **/
  var null_function = function() {};

  var get_query = function(href) {
    var vars = {}, hash,
        index_of_q = window.location.href.indexOf('?');

    if (index_of_q == -1) { return vars}

    var hashes = href.slice(index_of_q + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
      hash = hashes[i].split('=');
      vars[hash[0]] = decodeURIComponent(hash[1]);
    }
    return vars;
  }

  /** returns all key from obj_from that differ from obj_to */
  var get_diff_keys = function(obj_from, obj_to) {
    var diff = [];
    $.each(obj_from, function(key, val) {
      val && obj_to[key] != val && diff.push(key);
    });
    return diff;
  }

  var is_campaing_param = function(params) {
    var is_campaing_param = false;
    $.each(params, function(key, value) {
      if ($.inArray(key, CAMPAIGN_COOKIES)) {
        is_campaing_param = true;
      }
    });
    return is_campaing_param;
  }

  var get_user_currency = function(user_country_code) {
    return user_country_code.toLowerCase() === 'us' && 'USD' || 'EUR';
  };



  var augment_href = function(href) {
    var params = [];
    $.each(g_params, function(key, value) {
      value && params.push(key + '=' + encodeURIComponent(value));
    });

    return href + (/\?/.test(href) ? '&' : '?') + params.join('&');
  };


  /** handlers **/
  var onClick = function(e) {
    var href = $(this).attr('href'),
        that = this;

    if (href) {
      var is_link_to_shop = /talkingfriends/.test(href);
      var add_params = !g_cookies_enabled || is_link_to_shop;

      this.href = add_params && augment_href(href) || href;

      /* links google analytics to talkingshop domain */
      if (is_link_to_shop) {
        _gaq.push(['_link', that.href]); }
      return false;
    }
  }



  /** site currency related functions **/
  var set_user_currency = function(user_currency, user_country_code) {
      $('body')
        .removeClass('currency-usd')
        .removeClass('currency-gbp')
        .removeClass('currency-eur')
        .addClass('currency-' + user_currency.toLowerCase());
      $('.user-currency-code').text(user_currency);
      $('body').attr('data-curreny', user_currency);
      user_country_code && $('body').attr('data-country-code', user_country_code);
      $(document).trigger('changeUser', {currency:user_currency, country_code:user_country_code});
  }

  var ask_currency = function(user_currency) {
    $.fancybox({
      'overlayColor': '#002844',
      'type': 'inline',
      'href': '#storeRedirect',
      'margin': 5,
      'hideOnOverlayClick': false
    });
  };

  /* live events */
  $('currency-change').live('click', function() {
    var user_currency = $(this).attr('data-currency');
    $.cookie('user_currency', user_currency, { expires: 365, path: '/' });
    $.fancybox.close();
    set_user_currency(user_currency);
  });

  $('.user-currency').live('click', function() {
        $.fancybox({
          'overlayColor': '#002844',
          'type': 'inline',
          'href': '#currencyChange',
          'margin': 5
        });
  });



  var ui_tweaks = function() {
    var size_checker_display = $('#size-checker').css('display');

    if (size_checker_display === 'block' ||
        size_checker_display === 'inline-block') {

      $('.wrap').toggle(function() {
        $(this).find('.inner-text').animate(
          {left: '-100.5%'}, 800, null_function);
        $(this).addClass('open');
      }, function() {
        $(this).find('.inner-text').animate(
          {left: '0%'}, 800, null_function);
        $(this).removeClass('open');
      });

    } else if ($('#size-checker').css('display') === 'none') {

      $('.wrap').toggle(function() {
        $(this).next().slideDown('medium');
        $(this).addClass('open');
      }, function() {
        $(this).next().slideUp('medium');
        $(this).addClass('open');
      });

      window.scrollTo(0, 180);
    }


    var openGalery = function(event) {
      var src = $(this).attr('src'), number;
      try {
        number = parseInt(/_(\d+)\.jpg/.exec(src)[1]);
      } catch (e){
        number = 1;
      }

      $.fancybox($('#gallery'),{
        openEffect: 'none',
        closeEffect: 'none',
        afterShow : function() {
          $('#gallery').flexslider(number -1);
        },
        afterClose : function() {
          location.reload();
        }
      });

    };
  


    //$('.slider img').live( isTouchable() ? 'click' : 'click', openGalery); 
    $.fn.fancybox && $(".fancybox").fancybox({
      fitToView: true,
      autoResize: true,
      arrows: true,
      nextClick: true,
      preload: 4,
      scrolling:true
    });

    /** slider **/
    var SLIDER_DIMENSIONS = {
      'inline-block': 100,
      'none': 50
    };

    $.fn.flexslider && $('.carousel').flexslider({
        animation: 'slide',
        controlNav: false,
        directionNav: false,
        animationLoop: true,
        slideshow: false,
        itemWidth: 222,
        itemMargin: SLIDER_DIMENSIONS[size_checker_display] || 200,
        maxItems: 4
    });

    /*var gallery_slider = $('#gallery').flexslider({
      animation: 'slide',
      controlNav:true,
      directionNav: true,
      animationLoop: false,
      itemWidth:600,
      slideshow: false,
      maxItems: 1
    });*/

    $('.stars_selection ul li span').hover(function() {
      $(this).prev().css('color', '#fff');
    }, function() {
      $(this).prev().css('color', '#01467E');
    });
  }

  /** Each element with class track-scroll or track-click
   * must have attribute data-event which will be send as
   * event identificator
   */

  var isTouchable = function() {
    return 'ontouchstart' in document.documentElement;
  }

  var send_click_event = function(el) {
      var
        parts
        parents = $(el).parents('*[data-event]'),
        data = $(el).attr('data-event') || parents.length && parents[0].getAttribute('data-event');


      if (data) {
        params = ['_trackEvent', null, null, null, null, true]
        $.each(data.split(";"), function(i, s) {
          params[i+1] = i < 3 && $.trim(s) || s})
        //_gaq.push(['_trackEvent', 'Videos', 'Play', 'Baby\'s First Birthday'])
        _gaq.push(params);
      }
  }

  var register_events = function() {

    // timer events
    $.each([5, 10, 15, 20], function(index, value) {
      window.setTimeout(function() {
        _gaq.push(['_trackEvent', 'Timer', 'Elapsed', value + 's', null, true]);
      }, value * 1000);
    });

    // report special click
    $('.track-click').live('click', function(event){
      send_click_event(event.target)});

    // report if element is scrolled into view
    $(window).scroll(function() {
      $('.track-scroll').each(function() {
        var data = $(this).attr('data-event');
        if (isScrolledIntoView(this) && !(data in g_tracked_events)) {
          _gaq.push(['_trackEvent', 'Scroll', data]);
          g_tracked_events[data] = true;
      }
      });
    });
  }

  var isScrolledIntoView = function(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return elemBottom <= docViewBottom;
  }

// responsive image handler
var responsiveImageHandler = function() {
  var 
    $this = $(this),
    data_images = $this.attr('data-images') || '', 
    original_src = $this.attr('src') || '', 
    images = [];

  $.each(data_images.split(','), function(idx, val) {
    var a = $.trim(val).split(' '),
        max_width = parseInt($.trim(a[0])),
        image_postfix = $.trim(a[1]);
    images.push([max_width, image_postfix]);
  });


  // inserts postfix to image
  var insert_postfix = function(url, postfix) {
    var parts = /^(.*)(\.\w+)(\?.*)*$/.exec(url),
        new_parts = [];

    if (parts) {
      new_parts.push(parts[1]);
      new_parts.push(postfix);
      new_parts.push(parts[2]);
      parts[3] && new_parts.push(parts[3]);

      return new_parts.join('');
    } else {
      return url;
    }
  }
  var optimalSrc = function(width) {
    var src = original_src,
        current_width = 99999;

    $.each(images, function(idx, val) {
      var img_width = val[0], img_postfix = val[1];
      if (img_width >= width && img_width < current_width) {
        src = insert_postfix(original_src, img_postfix);
        current_width = img_width;
      }
    });
    return src;
  }

  var resize = function() {
    var width = $(window).width(),
        src = optimalSrc(width);
    $this.attr('src', src);
    $this.attr('title', src);
  }
  resize();
  $(window).resize(resize);
}


$(".responsive-image").each(responsiveImageHandler);
main();
if (window.innerWidth < 800) {
  setTimeout(function() {
    $('.autoscroll-to').each(function(){    
    $('body').scrollTop(
      $(this).offset().top+15)})}, 600);
};

// scrolls to autoscroll-to element if client width <= 767px
}


})(jQuery);

