## Installation

## TODO

1) Responsive design for small resolutions. Do it relative to base font with ems.
2) Fire proper GA events when user clicks on button.

## GA

How to check if xdomain is working?

Clear your browser and go to 1st domain. Look at the cookie `__utma`. It looks like:

    153192107.1009911975.1358506315.1358506315.1358506315.1

Remember the number after the 1st period (2nd number). This is `visitor_id`. 
It is enough to remember just first and last two digits.

Now click or post form to get to the other site. Check `__utma` again. The `visitor_id` must be
equal to `visitor_id` from the 1st site.

Here you cna read more about [GA cookies and their meaning][ga2]. Check also the [article on Beaconfire site][ga1].


[ga1]: http://www.beaconfire.com/blog/2012/10/how-to-check-whether-you-are-tracking-visitors-across-your-websites-in-google-analytics-cross-domain-tracking/
[ga2]: http://blog.vkistudios.com/index.cfm/2010/8/31/GA-Basics-The-Structure-of-Cookie-Values

## Plugins

Stick to recomendations about [authoring jQuery plugin][j4].

[j1]: https://github.com/brandonaaron/jquery-mousewheel.git
[j2]: https://github.com/carhartl/jquery-cookie.git
[j3]: http://fancyapps.com/fancybox/
[j4]: http://docs.jquery.com/Plugins/Authoring
[j5]: http://www.netmagazine.com/features/five-steps-gettin-flexy-responsive-web-design
### OSx

Install node and npm package manageer:

    brew install node
    curl https://npmjs.org/install.sh | sh

Install `less` through `npm`:

    npm install -g less

[1] http://shapeshed.com/setting-up-nodejs-and-npm-on-mac-osx/
[2] http://lesscss.org/
[3] https://github.com/ixti/jekyll-assets/
[4] http://matthodan.com/2012/11/22/jekyll-asset-pipeline.html
[5] http://stackoverflow.com/questions/6644654/parse-url-with-jquery-javascript
